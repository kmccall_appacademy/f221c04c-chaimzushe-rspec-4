class Timer
  attr_accessor :seconds
   def initialize
     @seconds = 0
   end

   def time_string
     seconds_remainder = @seconds % 60
     seconds_str = seconds_remainder >= 10 ? seconds_remainder.to_s : "0" + seconds_remainder.to_s
     all_minutes = seconds / 60
     minutes_remainder = ((seconds / 60) % 60).floor
     minutes_str = minutes_remainder >= 10 ? minutes_remainder.to_s : "0" + minutes_remainder.to_s
     hours = all_minutes / 60
     hours_str = hours >= 10 ? hours.to_s : "0" + hours.to_s
     hours_str + ":" + minutes_str + ":" + seconds_str
   end

end
