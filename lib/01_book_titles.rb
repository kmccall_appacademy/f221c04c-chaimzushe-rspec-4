class Book
  # TODO: your code goes here!
  attr_accessor :title


  def title
    prepositions = ["in", "the", "of", "a" ,"in", "an", "and"]
    titalized = []
    @title.split(" ").map.with_index do |word, index|
      prepositions.include?(word) && index != 0 ? word : word.capitalize
    end.join(" ")
  end
  
end
