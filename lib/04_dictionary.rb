class Dictionary
  attr_reader :dictionary
  # TODO: your code goes here!
  def initialize
   @dictionary = {}
  end

  def entries
   @dictionary
  end

  def keywords
    @dictionary.keys.sort
  end

  def add(hash={})
     hash.each { |k, v| dictionary[k] = v } if hash.class == Hash
     dictionary[hash] = nil unless hash.class == Hash
  end

  def find(query_key)
    @dictionary.select{|k,v| k.include?(query_key)}
  end

  def printable
    str = ""
    sorted = Hash[dictionary.sort]
    sorted.each do |k, v|
      str +=  "[#{k}] \"#{v}\"\n"
     end
    str.chomp
  end

  def include?(key)
    @dictionary.has_key?(key)
  end
end
