class Temperature
  attr_reader :fahrenheit, :celcious
  # TODO: your code goes here!
  def initialize(hash={})
     @fahrenheit = hash[:f]
     @celcious = hash[:c]
  end

  def in_fahrenheit
    @fahrenheit || (@celcious * 9).fdiv(5) + 32
  end

  def in_celsius
     @celcious || (@fahrenheit - 32) * 5 / 9
  end

  def self.from_celsius(num)
    Temperature.new(:c => num)
  end

  def self.from_fahrenheit(num)
    Temperature.new(:f => num)
  end
end

class Celsius < Temperature

  def initialize(celcious)
    @celcious = celcious
  end

end

class Fahrenheit < Temperature

  def initialize(fahrenheit)
    @fahrenheit = fahrenheit
  end

end
